import unittest
from vehiculo import Vehiculo
class TestCoche(unittest.TestCase):
    def test_acelerar(self):
        coche = Vehiculo("rojo", "Toyota", "Corolla", "1234-ABC")
        coche.acelerar(20)
        self.assertEqual(coche.velocidad, 20)

    def test_frenar(self):
        coche = Vehiculo("azul", "Ford", "Fiesta", "5678-DEF")
        coche.velocidad = 30
        coche.frenar(10)
        self.assertEqual(coche.velocidad, 20)

    def test_frenar_no_negativo(self):
        coche = Vehiculo("verde", "Honda", "Civic", "9012-GHI")
        coche.velocidad = 5
        coche.frenar(10)  # Se intenta frenar más de lo que se puede
        self.assertEqual(coche.velocidad, 0)

if __name__ == '__main__':
    unittest.main()
